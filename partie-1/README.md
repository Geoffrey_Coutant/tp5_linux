# TP5 : Partie 1 : Mise en place et maîtrise du serveur Web

## 1. Installation

***Installer le serveur Apache***

```
[user1@localhost ~]$ sudo dnf install httpd php

Installed:
  apr-1.7.0-11.el9.aarch64                apr-util-1.6.1-20.el9.aarch64        
  apr-util-bdb-1.6.1-20.el9.aarch64       apr-util-openssl-1.6.1-20.el9.aarch64
  httpd-2.4.53-7.el9.aarch64              httpd-core-2.4.53-7.el9.aarch64      
  httpd-filesystem-2.4.53-7.el9.noarch    httpd-tools-2.4.53-7.el9.aarch64     
  libxslt-1.1.34-9.el9.aarch64            mailcap-2.1.49-5.el9.noarch          
  mod_http2-1.15.19-2.el9.aarch64         mod_lua-2.4.53-7.el9.aarch64         
  nginx-filesystem-1:1.20.1-13.el9.noarch oniguruma-6.9.6-1.el9.5.aarch64      
  php-8.0.20-3.el9.aarch64                php-cli-8.0.20-3.el9.aarch64         
  php-common-8.0.20-3.el9.aarch64         php-fpm-8.0.20-3.el9.aarch64         
  php-mbstring-8.0.20-3.el9.aarch64       php-opcache-8.0.20-3.el9.aarch64     
  php-pdo-8.0.20-3.el9.aarch64            php-xml-8.0.20-3.el9.aarch64         
  rocky-logos-httpd-90.13-1.el9.noarch   

Complete!

[user1@localhost ~]$ sudo mkdir /etc/httpd/sites-available
[user1@localhost ~]$ sudo mkdir /etc/httpd/sites-enabled
[user1@localhost ~]$ sudo mkdir /var/www/sub-domains/
[user1@localhost ~]$ vi /etc/httpd/conf/httpd.conf
```

***Démarrer le service Apache***

```
[user1@localhost ~]$ sudo systemctl start httpd
[user1@localhost ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor pr>
    Drop-In: /usr/lib/systemd/system/httpd.service.d
             └─php-fpm.conf
     Active: active (running) since Tue 2023-01-03 16:10:26 CET; 8s ago
       Docs: man:httpd.service(8)
   Main PID: 1396 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 5876)
     Memory: 36.9M
        CPU: 69ms
     CGroup: /system.slice/httpd.service
             ├─1396 /usr/sbin/httpd -DFOREGROUND
             ├─1403 /usr/sbin/httpd -DFOREGROUND
             ├─1404 /usr/sbin/httpd -DFOREGROUND
             ├─1405 /usr/sbin/httpd -DFOREGROUND
             └─1406 /usr/sbin/httpd -DFOREGROUND

Jan 03 16:10:26 localhost.localdomain systemd[1]: Starting The Apache HTTP Serv>
Jan 03 16:10:26 localhost.localdomain httpd[1396]: AH00558: httpd: Could not re>
Jan 03 16:10:26 localhost.localdomain httpd[1396]: Server configured, listening>
Jan 03 16:10:26 localhost.localdomain systemd[1]: Started The Apache HTTP Serve>
```

***TEST***

```
httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor pre>
    Drop-In: /usr/lib/systemd/system/httpd.service.d
             └─php-fpm.conf
     Active: active (running) since Tue 2023-01-03 16:10:26 CET; 9min ago
       Docs: man:httpd.service(8)
   Main PID: 1396 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes>
      Tasks: 213 (limit: 5876)
     Memory: 36.9M
        CPU: 1.498s
     CGroup: /system.slice/httpd.service
             ├─1396 /usr/sbin/httpd -DFOREGROUND
             ├─1403 /usr/sbin/httpd -DFOREGROUND
             ├─1404 /usr/sbin/httpd -DFOREGROUND
             ├─1405 /usr/sbin/httpd -DFOREGROUND
             └─1406 /usr/sbin/httpd -DFOREGROUND

Jan 03 16:10:26 localhost.localdomain systemd[1]: Starting The Apache HTTP Serv>
Jan 03 16:10:26 localhost.localdomain httpd[1396]: AH00558: httpd: Could not re>
Jan 03 16:10:26 localhost.localdomain httpd[1396]: Server configured, listening>
Jan 03 16:10:26 localhost.localdomain systemd[1]: Started The Apache HTTP Serve>

[user1@localhost ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      
      html {
        height: 100%;
        width: 100%;
      }  
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(23,43,70,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(23,43,70,1) 30%, rgba(0,0,0,1) 90%) ;
```

```
radiou22@MacBook-Air-de-jojo ~ % curl 192.168.212.14
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
      
      html {
        height: 100%;
        width: 100%;
      }  
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(23,43,70,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(23,43,70,1) 30%, rgba(0,0,0,1) 90%) ;
```

## 2. Avancer vers la maîtrise du service

```
[user1@localhost ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#	[Service]
#	Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

***Déterminer sous quel utilisateur tourne le processus Apache***

```
[user1@localhost ~]$ cat /etc/httpd/conf/httpd.conf
ServerRoot "/etc/httpd"
Listen 80
Include conf.modules.d/*.conf
User apache
Group apache
ServerAdmin root@localhost
[user1@localhost ~]$ ps -ef | grep apache
apache      1398    1397  0 16:10 ?        00:00:00 php-fpm: pool www
apache      1399    1397  0 16:10 ?        00:00:00 php-fpm: pool www
apache      1400    1397  0 16:10 ?        00:00:00 php-fpm: pool www
apache      1401    1397  0 16:10 ?        00:00:00 php-fpm: pool www
apache      1402    1397  0 16:10 ?        00:00:00 php-fpm: pool www
apache      1403    1396  0 16:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1404    1396  0 16:10 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache      1405    1396  0 16:10 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache      1406    1396  0 16:10 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
user1       1702     887  0 16:46 pts/0    00:00:00 grep --color=auto apache
[user1@localhost testpage]$ ls -al
total 12
drwxr-xr-x.  2 root root   24 Jan  3 15:30 .
drwxr-xr-x. 89 root root 4096 Jan  3 15:30 ..
-rw-r--r--.  1 root root 7620 Jul 27 20:05 index.html
```

***Changer l'utilisateur utilisé par Apache***

```
[user1@localhost etc]$ sudo useradd user2 -s /sbin/nologin --home-dir /usr/share/httpd
useradd: warning: the home directory /usr/share/httpd already exists.
useradd: Not copying any file from skel directory into it.
[user1@localhost etc]$ cat passwd
user2:x:1002:1002::/usr/share/httpd:/sbin/nologin
[user1@localhost conf]$ cat httpd.conf
ServerRoot "/etc/httpd"
Listen 80
Include conf.modules.d/*.conf
User user2
Group user2
ServerAdmin root@localhost
[user1@localhost conf]$ sudo systemctl restart httpd
```

***Faites en sorte que Apache tourne sur un autre port***

```
[user1@localhost conf]$ cat httpd.conf 
ServerRoot "/etc/httpd"
Listen 800
Include conf.modules.d/*.conf
User user2
Group user2
ServerAdmin root@localhost
[user1@localhost conf]$ sudo firewall-cmd --add-port 800/tcp --permanent 
success
[user1@localhost conf]$ sudo firewall-cmd --remove-port 80/tcp --permanent 
success
[user1@localhost conf]$ sudo systemctl restart httpd
[user1@localhost conf]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor pre>
    Drop-In: /usr/lib/systemd/system/httpd.service.d
             └─php-fpm.conf
     Active: active (running) since Tue 2023-01-03 17:24:06 CET; 14s ago
       Docs: man:httpd.service(8)
   Main PID: 2008 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes>
      Tasks: 213 (limit: 5876)
     Memory: 38.4M
        CPU: 103ms
     CGroup: /system.slice/httpd.service
             ├─2008 /usr/sbin/httpd -DFOREGROUND
             ├─2009 /usr/sbin/httpd -DFOREGROUND
             ├─2010 /usr/sbin/httpd -DFOREGROUND
             ├─2011 /usr/sbin/httpd -DFOREGROUND
             └─2012 /usr/sbin/httpd -DFOREGROUND

Jan 03 17:24:06 localhost.localdomain systemd[1]: Starting The Apache HTTP Serv>
Jan 03 17:24:06 localhost.localdomain httpd[2008]: AH00558: httpd: Could not re>
Jan 03 17:24:06 localhost.localdomain systemd[1]: Started The Apache HTTP Serve>
Jan 03 17:24:06 localhost.localdomain httpd[2008]: Server configured, listening>
[user1@localhost conf]$ ss -alnpt
State     Recv-Q    Send-Q       Local Address:Port        Peer Address:Port    Process    
LISTEN    0         128                0.0.0.0:22               0.0.0.0:*                  
LISTEN    0         128                   [::]:22                  [::]:*                  
LISTEN    0         511                      *:800                    *:*  
```
